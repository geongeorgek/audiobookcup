/** @format */

const banner = require("cli-banner");
const prompts = require("prompts");
const cliProgress = require("cli-progress");

const downloadFile = require("./utils/downloader");
const getData = require("./utils/fileInfo");
const { clear, makeDir, safeString } = require("./utils/general");

const getInput = async () => {
  const url = await prompts({
    type: "text",
    name: "htmlUrl",
    message: "Enter the audiobook url:",
  });
  return url;
};

const main = async () => {
  console.log(banner("Audiobookcup.com Downloader", { borderColor: "green" }));
  const { htmlUrl } = await getInput();
  clear();
  console.log("Fetching page data...");
  try {
    const { title, url } = await getData(htmlUrl);
    console.log("Starting download...");
    const path = `./downloads/${safeString(title)}`;
    makeDir(path);
    const mp3Path = `${path}/${safeString(title)}.mp3`;

    const bar = new cliProgress.SingleBar(
      {},
      cliProgress.Presets.shades_classic
    );
    bar.start(100, 0);
    downloadFile(
      url,
      mp3Path,
      ({ percent }) => {
        bar.update(Math.round(percent * 100));
      },
      () => {
        bar.stop();
        console.log(`Download complete!`);
        console.log(path);
      }
    );
  } catch (e) {
    console.log("Some error occured", e);
  }
};

main();
