/** @format */

const request = require("request");

const titleRegex = /<title>(.*?)<\/title>/i;
const base64Regex = /sourcePath:'encrypt:(.*?)'/i;

function getMatch(input, regex) {
  if (regex.test(input)) {
    const matches = input.match(regex);
    return matches[1] || "";
  } else {
    return "";
  }
}

function getBody(url) {
  return new Promise((resolve, reject) => {
    request(url, function (error, response, body) {
      if (response.statusCode === 200) {
        resolve(body);
      } else {
        reject(error);
      }
    });
  });
}

async function getData(pageUrl) {
  const body = await getBody(pageUrl);
  const title = getMatch(body, titleRegex);
  const urlBase64 = getMatch(body, base64Regex);
  const url = Buffer.from(urlBase64, "base64").toString("utf-8");

  return { title, url };
}

module.exports = getData;
