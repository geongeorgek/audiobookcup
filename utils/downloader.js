/** @format */

const request = require("request");
const fs = require("fs");
const progress = require("request-progress");

const headers = {
  Connection: "keep-alive",
  Pragma: "no-cache",
  "Cache-Control": "no-cache",
  "sec-ch-ua":
    '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
  "sec-ch-ua-mobile": "?0",
  "User-Agent":
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36",
  Accept: "*/*",
  Origin: "https://www.audiobookcup.com",
  "Sec-Fetch-Site": "cross-site",
  "Sec-Fetch-Mode": "cors",
  "Sec-Fetch-Dest": "audio",
  Referer: "https://www.audiobookcup.com/",
  "Accept-Language": "en-US,en;q=0.9,ml-IN;q=0.8,ml;q=0.7",
  Range: "bytes=0-",
};

function downloadFile(
  url,
  outputPath,
  onProgress = () => {},
  onEnd = () => {},
  onError = () => {}
) {
  const file = fs.createWriteStream(outputPath);
  const options = {
    url: url,
    headers: headers,
  };

  progress(request(options))
    .on("progress", onProgress)
    .on("error", onError)
    .on("end", onEnd)
    .pipe(file);
}

module.exports = downloadFile;
