/** @format */

const fs = require("fs");
const readline = require("readline");
const UrlSafeString = require("url-safe-string");

const safeGenerator = new UrlSafeString();

const toHHMMSS = () => {
  const sec_num = parseInt(this, 10); // don't forget the second param
  const hours = Math.floor(sec_num / 3600);
  const minutes = Math.floor((sec_num - hours * 3600) / 60);
  const seconds = sec_num - hours * 3600 - minutes * 60;

  if (hours < 10) {
    hours = "0" + hours;
  }
  if (minutes < 10) {
    minutes = "0" + minutes;
  }
  if (seconds < 10) {
    seconds = "0" + seconds;
  }
  return hours + ":" + minutes + ":" + seconds;
};

const makeDir = (path) => {
  return !fs.existsSync(path) && fs.mkdirSync(path, { recursive: true });
};

const clear = () => {
  const blank = "\n".repeat(process.stdout.rows);
  console.log(blank);
  readline.cursorTo(process.stdout, 0, 0);
  readline.clearScreenDown(process.stdout);
};

const safeString = (str) => {
  return safeGenerator.generate(str);
};

module.exports = { toHHMMSS, makeDir, clear, safeString };
