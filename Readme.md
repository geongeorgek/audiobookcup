<!-- @format -->

# Audiobookcup.com Downloader

1. Clone this repo
2. `yarn` or `npm install`
3. `node app.js`

# Usage

- Use urls like `https://www.audiobookcup.com/the-autobiography-of-benjamin-franklin-amazonclassics-edition-audiobook/`
- Check the `downloads` folder inside the project after download
